# Circulo for iOS Changelog

## 1.2.0

- Updated to latest Matrix SDK 0.27.13.
- Major UI rework:
  - Improved alert UI
  - Improved location sharing
  - Improved alert tracking and response
  - Improved location tracking
  - Fixed dark mode display
  - Added standard room chat for communication outside predefined flow. 


## 1.1.0

- Updated to latest Matrix SDK 0.27.3.
- Improved background updates.
- Fixed broken push notification handling.
- Added support for Apple's "Critical Alerts": When accepted, 
  urgent notifications will show, even when device is in do-not-disturb mode.
- Advertise in-app notification settings in Settings app's notification section.
- Added passive support for new Matrix threads.
- Allow location sharing from everybody always.
- Removed confusing "slashed cloud" icon for users which weren't seen in a long time.

## 1.0.8
- Fixed crash.
- Supports dark mode.
- Tap images for full size and sharing.

## 1.0.7

- Stability improvements: Updated to latest Keanu and Matrix SDK 0.26.4.
- Fixed auto-generated password storing.
- Fixed room member sync after join.
- Fixed circle scene UI when rotating screen.
- Added localization from Android version. 
- Added optional measurement study.
- Try to indicate when the device is offline, and when a circle member is offline.
