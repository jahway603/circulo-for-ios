//
//  PopupNotification.swift
//  Circulo
//
//  Created by Benjamin Erhart on 04.04.24.
//

import UIKit

class PopupNotification: UIView {

    var labelText: String? {
        get {
            label.text
        }
        set {
            label.text = newValue
        }
    }


    // MARK: Private Properties

    private lazy var body: UIView = {
        let body = UIView(frame: .zero)
        body.translatesAutoresizingMaskIntoConstraints = false
        body.cornerRadius = 8
        body.backgroundColor = .popup

        return body
    }()

    private lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .preferredFont(forTextStyle: .body)

        return label
    }()

    private lazy var button: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white)
        button.setTitle("OK".localize())

        return button
    }()

    private lazy var arrow: UIView = {
        let arrow = TriangleView(frame: .zero)
        arrow.translatesAutoresizingMaskIntoConstraints = false
        arrow.tintColor = .popup
        arrow.isOpaque = false

        return arrow
    }()

    private var closingTimer: Timer?


    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setup()
    }


    // MARK: Public Methods

    func attach(to view: UIView) {
        guard let container = view.superview else {
            return
        }

        container.addSubview(self)
        bottomAnchor.constraint(equalTo: view.topAnchor, constant: -8).isActive = true
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        leadingAnchor.constraint(greaterThanOrEqualTo: container.leadingAnchor, constant: 8).isActive = true
        trailingAnchor.constraint(lessThanOrEqualTo: container.trailingAnchor, constant: -8).isActive = true
    }

    func show() {
        alpha = 0
        isHidden = false

        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.alpha = 1
        } completion: { [weak self] _ in
            let timer = Timer(timeInterval: 5, repeats: false) { _ in
                self?.hide()
            }
            self?.closingTimer = timer

            RunLoop.main.add(timer, forMode: .common)
        }
    }

    @objc func hide() {
        closingTimer?.invalidate()
        closingTimer = nil

        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.alpha = 0
        } completion: { [weak self] _ in
            self?.isHidden = true
            self?.alpha = 1
        }
    }


    // MARK: Private Methods

    private func setup() {
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false

        addSubview(body)
        body.topAnchor.constraint(equalTo: topAnchor).isActive = true
        body.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        body.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true

        body.addSubview(label)
        label.topAnchor.constraint(equalTo: body.topAnchor, constant: 16).isActive = true
        label.leadingAnchor.constraint(equalTo: body.leadingAnchor, constant: 16).isActive = true
        label.bottomAnchor.constraint(equalTo: body.bottomAnchor, constant: -16).isActive = true

        body.addSubview(button)
        button.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 16).isActive = true
        button.trailingAnchor.constraint(equalTo: body.trailingAnchor, constant: -16).isActive = true
        button.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        button.addTarget(self, action: #selector(hide), for: .touchUpInside)

        addSubview(arrow)
        arrow.topAnchor.constraint(equalTo: body.bottomAnchor).isActive = true
        arrow.heightAnchor.constraint(equalToConstant: 12).isActive = true
        arrow.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        arrow.widthAnchor.constraint(equalToConstant: 24).isActive = true
        arrow.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
}
