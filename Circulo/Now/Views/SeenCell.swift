//
//  SeenCell.swift
//  Circulo
//
//  Created by Benjamin Erhart on 23.02.24.
//

import UIKit
import KeanuCore

class SeenCell: UICollectionViewCell {

    class var identifier: String {
        String(describing: self)
    }

    class var nib: UINib {
        UINib(nibName: identifier, bundle: nil)
    }

    @IBOutlet weak var avatarView: AvatarView!

    @IBOutlet weak var nameLb: UILabel!


    @discardableResult
    func set(_ member: Member) -> Self {
        avatarView.load(member)

        nameLb.text = member.displayName

        return self
    }
}
