//
//  AlertButton.swift
//  Circulo
//
//  Created by Benjamin Erhart on 07.02.24.
//

import UIKit
import Keanu

class AlertButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setup()
    }

    private func setup() {
        let image = UIImage(named: "ic_notif_all", in: .init(for: WorkingOverlay.self), with: nil)?
            .withRenderingMode(.alwaysTemplate)

        setImage(image)
    }
}
