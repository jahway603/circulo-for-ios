//
//  AlertView.swift
//  Circulo
//
//  Created by Benjamin Erhart on 16.05.24.
//

import UIKit
import KeanuCore

class AlertView: UIView {

    class var nib: UINib {
        UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    class func create() -> AlertView {
        nib.instantiate(withOwner: nil).first as! AlertView
    }


    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var subtitleLb: UILabel!

    @IBOutlet weak var button: UIButton! {
        didSet {
            button.setTitle("Show".localize())
        }
    }

    var member: Member?


    func apply(_ member: Member) {
        self.member = member

        titleLb.text = "Alert from %".localize(value: member.displayName)

        if let resolvedtime = member.lastAlert?.resolvedTime?.date {
            backgroundColor = .conditionSafe
            icon.backgroundColor = .systemGreen

            button.setTitleColor(.white)
            button.tintColor = .conditionSafe

            subtitleLb.text = "Resolved %".localize(value: Formatters.format(friendlyTimestamp: resolvedtime))
        }
        else {
            backgroundColor = .alert
            icon.backgroundColor = .alertDark

            button.setTitleColor(.alert)
            button.tintColor = .white

            let ts = member.lastAlert?.timestamp.date ?? .init(timeIntervalSince1970: 0)
            subtitleLb.text = Formatters.format(friendlyTimestamp: ts)
        }
    }

    @IBAction func tap() {
        let vc = OtherAlertViewController()
        vc.member = member

        nextUiViewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
