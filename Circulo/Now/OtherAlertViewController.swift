//
//  OtherAlertViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 17.05.24.
//

import UIKit
import KeanuCore
import MapKit
import Keanu

class OtherAlertViewController: UIViewController, AudioPlayerDelegate {

    var member: Member?


    @IBOutlet weak var avatar: AvatarView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var audioContainer: UIView!
    @IBOutlet weak var durationLb: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var playPauseBt: UIButton!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var infoLb: UILabel!
    @IBOutlet weak var ackBt: UIButton!


    private var player: AudioPlayer?
    private var overlays = [MKOverlay]()

    private lazy var workingOverlay: WorkingOverlay = {
        WorkingOverlay().addToSuperview(view)
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        avatar.load(member)

        titleLb.text = "Alert from %".localize(value: member?.displayName ?? "")
        navigationItem.title = titleLb.text

        if let alert = member?.lastAlert {
            player = AudioPlayer(alert)
            player?.delegate = self
        }

        let duration = player?.duration ?? 0

        if duration > 0 {
            durationLb.text = Formatters.format(mediaDuration: player?.duration ?? 0)
            progress.progress = 0
        }
        else {
            audioContainer.isHidden = true
            audioContainer.heightConstraint = 0
        }

        CirculoAnnotationView.member = member
        map.register(CirculoAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)

        updateMap()

        updateAckBt()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(update), name: .memberChanged, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)

        player?.stop()
    }


    // MARK: Actions

    @IBAction func playPause() {
        if let error = player?.error {
            AlertHelper.present(self, message: error.localizedDescription)

            return
        }

        if player?.isPlaying ?? false {
            playPauseBt.setImage(.init(systemName: "play.circle"))

            player?.stop()

            progress.progress = 0
        }
        else {
            playPauseBt.setImage(.init(systemName: "stop.circle"))

            player?.play()
        }
    }

    @IBAction func showMap() {
        let vc = CurrentLocationViewController()
        vc.member = member

        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func ack() {
        guard !(member?.haveRecognizedAlert ?? true) else {
            return
        }

        workingOverlay.isHidden = false

        Task {
            do {
                try await member?.lastAlert?.recognize()
                try await member?.locationTrack?.recognize()

                await MainActor.run {
                    workingOverlay.isHidden = true
                    updateAckBt()
                }
            }
            catch {
                await MainActor.run {
                    workingOverlay.isHidden = true
                    AlertHelper.present(self, message: error.localizedDescription)
                }
            }
        }
    }


    // MARK: AudioPlayerDelegate

    func audioPlayerDidFinish() {
        progress.progress = 0
        playPauseBt.setImage(.init(systemName: "play.circle"))
    }

    func audioPlayerError(_ error: any Error) {
        AlertHelper.present(self, message: error.localizedDescription)
    }

    func audioPlayerTimeUpdate(_ duration: TimeInterval, _ currentTime: TimeInterval) {
        progress.progress = Float(currentTime / duration)
    }


    // MARK: Private Methods

    @objc
    private func update(_ notification: Notification) {
        if notification.object as? Member == member {
            updateMap()
            updateAckBt()
        }
    }

    private func updateMap() {
        overlays = map.update(track: member?.locationTrack?.locations ?? [], old: overlays)

        if let overlay = overlays.last {
            var rect = MKCoordinateRegion(overlay.boundingMapRect)
            rect.span.latitudeDelta *= 1.2
            rect.span.longitudeDelta *= 1.2

            map.setRegion(rect, animated: true)
            map.isHidden = false
        }
        else {
            map.isHidden = true
        }
    }

    private func updateAckBt() {
        if member?.haveRecognizedAlert ?? false {
            ackBt.setTitle("Confirmation Sent".localize())
            ackBt.setTitleColor(.systemBackground)
            ackBt.tintColor = .conditionSafe
        }
        else {
            ackBt.setTitle("Got It".localize())
            ackBt.setTitleColor(.systemBackground)
            ackBt.tintColor = .accent
        }

        infoLb.text = "% people have seen this alert. Tap the button to let % know you've seen it.".localize(
            values: Formatters.format(int: member?.lastAlert?.countRecognized ?? 0), member?.displayName ?? "")
    }
}
