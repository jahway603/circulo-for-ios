//
//  AppDelegate.swift
//  Circulo
//
//  Created by N-Pex on 2019-09-17.
//

import UIKit
import KeanuCore
import Keanu
import AFNetworking

@UIApplicationMain
class AppDelegate: BaseAppDelegate {

    private var setupDone = false

    override var router: Router {
        CirculoRouter.shared
    }


    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setUp()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    override func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        setUp()
        return super.application(application, performFetchWithCompletionHandler: completionHandler)
    }
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        setUp()
        super.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        AFNetworkReachabilityManager.shared().startMonitoring()
    }

    override func applicationDidEnterBackground(_ application: UIApplication) {
        super.applicationDidEnterBackground(application)

        AFNetworkReachabilityManager.shared().stopMonitoring()

        CleanInsightsManager.shared.persist()
    }


    // MARK: Private Methods

    private func setUp() {
        guard !setupDone else {
            return
        }

        setupDone = true

        let _ = UITheme.shared // Initialize theme
        MXKAccount.enableNotificationServiceExtension()
        MXKAppSettings.standard().notificationBodyLocalizationKey = "Incoming event".localize()
        PushManager.shared = CirculoPushManager()
        KeanuCore.setUp(with: Config.self)
        KeanuCore.setUpLocalization(fileName: "Localizable", bundle: Bundle.main)
        UrlHandler.shared.register(path: Config.universalLinkHost, handler: handleUniversalLink(url:))

        MXSDKOptions.sharedInstance().enableThreads = true
    }
    
    private func handleUniversalLink(url: URL) -> Bool {
        guard let id = url.fragment?.removingPercentEncoding,
              MXTools.isMatrixRoomIdentifier(id) || MXTools.isMatrixRoomAlias(id)
        else {
            return false
        }

        let circleName = URLComponents(string: url.absoluteString)?.queryItems?
            .filter({$0.name == "name"}).first?.value

        UIApplication.shared.popToChatListViewController()

        if let navC = UIApplication.shared.mainVc?.viewControllers?.first as? UINavigationController, 
            let vc = navC.viewControllers.first as? HomeViewController
        {
            vc.didReceiveCircleInvite(id: id, circleName: circleName)
        }

        return true
    }
}

