//
//  CleanInsightsManager.swift
//  Circulo
//
//  Created by Benjamin Erhart on 17.04.23.
//

import Foundation
import CleanInsightsSDK

class CleanInsightsManager {

    private static let campaignId = "circulo-usage"


    static let shared = CleanInsightsManager()


    private let ci: CleanInsights? = {
        // WTF? The configuration file is bundled and never modified, however, we get
        // crash reports from the field, where the app crashes here, by all means.
        // Highly annoying. So, we need to make this optional.
        if let file = Bundle.main.url(forResource: "cleaninsights", withExtension: "json") {
            return try? CleanInsights(jsonConfigurationFile: file)
        }

        return nil
    }()


    private init() {
    }

    var isActive: Bool {
        guard let campaign = ci?.conf.campaigns[Self.campaignId] else {
            return false
        }

        let now = Date()

        return now >= campaign.start && now <= campaign.end
    }

    var isGranted: Bool {
        switch ci?.consent(forCampaign: Self.campaignId)?.state {
        case .granted, .notStarted:
            return true

        default:
            return false
        }
    }

    var remainingConsentDays: Int {
        guard let end = ci?.consent(forCampaign: Self.campaignId)?.end else {
            return 0
        }

        return Calendar.current.dateComponents([.day], from: Date(), to: end).day ?? 0
    }

    var state: Consent.State {
        ci?.state(ofCampaign: Self.campaignId) ?? .unconfigured
    }

    private var groupCode: String {
        CirculoSettings.shared.cleanInsightsGroupCode ?? "action"
    }


    func measure(event action: String, name: String? = nil, value: Double? = nil) {
        ci?.measure(event: groupCode, action, forCampaign: Self.campaignId, name: name, value: value)
    }

    func persist() {
        ci?.persist()
    }

    func deny() {
        ci?.deny(campaign: Self.campaignId)
    }

    func grant() {
        ci?.grant(campaign: Self.campaignId)
    }
}
