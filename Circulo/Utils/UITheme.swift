//
//  Theme.swift
//  Circulo
//
//

import UIKit
import Keanu
import INSPhotoGallery

class UITheme: NSObject {
    
    /**
     Singleton instance.
     */
    public static let shared: UITheme = {
        return UITheme().setupAppearance()
    }()
    
    override init() {
    }
    
    // MARK: AppColors
    
    var mainThemeColor = UIColor.accent

    // MARK: AppAppearance
    
    func setupAppearance() -> UITheme {
        UIButton.appearance().tintColor = .systemGray

        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.isTranslucent = true
        navBarAppearance.tintColor = .label
        navBarAppearance.barTintColor = nil
        navBarAppearance.backgroundColor = nil
        navBarAppearance.shadowImage = UIImage()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.label]

        // On iOS 11 bar button items are descendants of button...
        UIButton.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = .label
        // ...on iOS 13 they are no longer that.
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = .accent
        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.accent], for: .normal)

        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = mainThemeColor

        // The photo scene is white-on-black. Revert our doings from above.
        UIButton.appearance(whenContainedInInstancesOf: [UINavigationBar.self, INSPhotosViewController.self]).tintColor = .white
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self, INSPhotosViewController.self]).tintColor = .white


        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.tintColor = .tabBarIcon
        tabBarAppearance.barTintColor = .tabBar
        tabBarAppearance.backgroundColor = .tabBar
        tabBarAppearance.unselectedItemTintColor = .label

        // Onboarding page control
        let pageControlAppearance = UIPageControl.appearance(whenContainedInInstancesOf: [CirculoAddAccountViewController.self])
        pageControlAppearance.pageIndicatorTintColor = .secondaryLabel
        pageControlAppearance.currentPageIndicatorTintColor = .darkGray
        pageControlAppearance.backgroundColor = .clear
        
        ImageButtonCell.appearance().labelColor = .label
        ImageButtonCell.appearance().iconColor = .settingsIcon

        return self
    }
}
