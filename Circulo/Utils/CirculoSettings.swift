//
//  CirculoSettings.swift
//  Circulo
//
//  Created by N-Pex on 01.12.21.
//

import Foundation

class CirculoSettings {

    public static let shared = CirculoSettings()


    private let defaults = UserDefaults(suiteName: Config.appGroupId)


    private init() {
    }


    public var notifyNewStatuses: String {
        get {
            defaults?.string(forKey: "notify_new") ?? "always"
        }
        set {
            defaults?.set(newValue, forKey: "notify_new")
        }
    }

    public var notifyUpdates: String {
        get {
            defaults?.string(forKey: "notify_update") ?? "always"
        }
        set {
            defaults?.set(newValue, forKey: "notify_update")
        }
    }

    public var notifyNormalSound: Bool {
        get {
            defaults?.object(forKey: "notify_normal_sound") != nil ? defaults!.bool(forKey: "notify_normal_sound") : true
        }
        set {
            defaults?.set(newValue, forKey: "notify_normal_sound")
        }
    }

    public var notifyUrgentSound: Bool {
        get {
            defaults?.object(forKey: "notify_urgent_sound") != nil ? defaults!.bool(forKey: "notify_urgent_sound") : true
        }
        set {
            defaults?.set(newValue, forKey: "notify_urgent_sound")
        }
    }

    public var cleanInsightsGroupCode: String? {
        get {
            defaults?.string(forKey: "ci_group_code")
        }
        set {
            defaults?.set(newValue, forKey: "ci_group_code")
        }
    }
}
