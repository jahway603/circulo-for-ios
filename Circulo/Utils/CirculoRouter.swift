//
//  CirculoTheme.swift
//  Circulo
//
//  Created by N-Pex on 2021-04-07.
//

import Keanu

public class CirculoRouter: Router {
    public func profileEdit() -> EditProfileViewController {
        return BaseRouter.shared.profileEdit()
    }
    
    public func profileNotifications() -> NotificationsViewController {
        return BaseRouter.shared.profileNotifications()
    }
    
    public func profilePreferences() -> PreferencesViewController {
        return BaseRouter.shared.profilePreferences()
    }
    
    public func profileAccount() -> AccountViewController {
        return CirculoAccountViewController()
    }
    
    public func profileAbout() -> AboutViewController {
        return BaseRouter.shared.profileAbout()
    }    

    /**
     Singleton instance.
     */
    public static let shared = CirculoRouter()

    open func welcome() -> WelcomeViewController {
        return CirculoWelcomeViewController(nibName: String(describing: CirculoWelcomeViewController.self), bundle: Bundle(for: type(of: self)))
    }

    open func addAccount() -> AddAccountViewController {
        return UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController() as! AddAccountViewController
    }

    open func enablePush() -> EnablePushViewController {
        return EnablePushViewController(nibName: "CirculoEnablePushViewController", bundle: Bundle(for: type(of: self)))
    }
    
    public func main() -> MainViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! MainViewController
    }

    public func chatList() -> ChatListViewController {
        return ChatListViewController(nibName: String(describing: ChatListViewController.self),
                                bundle: Bundle(for: type(of: self)))
    }

    public func discover() -> DiscoverViewController {
        return DiscoverViewController()
    }

    public func me() -> MeViewController {
        return MeViewController()
    }

    public func myProfile() -> MyProfileViewController {
        return MyProfileViewController()
    }
    
    open func room() -> RoomViewController {
        let vc = RoomViewController()
        vc.hidesBottomBarWhenPushed = true
        return vc
    }

    public func roomSettings() -> RoomSettingsViewController {
        return CirculoRoomSettingsViewController(nibName: String(describing: RoomSettingsViewController.self),
                                                 bundle: Bundle(for: BaseRouter.self))
    }

    open func profile() -> ProfileViewController {
        return ProfileViewController()
    }

    public func story() -> StoryViewController {
        return StoryViewController(nibName: String(describing: StoryViewController.self),
                                   bundle: Bundle(for: StoryViewController.self))
    }

    public func storyAddMedia() -> StoryAddMediaViewController {
        return StoryAddMediaViewController(nibName: String(describing: StoryAddMediaViewController.self),
                                           bundle: Bundle(for: StoryAddMediaViewController.self))
    }

    public func storyGallery() -> StoryGalleryViewController {
        return StoryGalleryViewController(nibName: String(describing: StoryGalleryViewController.self),
                                          bundle: Bundle(for: StoryGalleryViewController.self))
    }

    public func storyEditor() -> StoryEditorViewController {
        return StoryEditorViewController(nibName: String(describing: StoryEditorViewController.self),
                                         bundle: Bundle(for: StoryEditorViewController.self))
    }

    public func stickerPack() -> StickerPackViewController {
        return StickerPackViewController()
    }

    public func pickSticker() -> PickStickerViewController {
        return PickStickerViewController(nibName: String(describing: PickStickerViewController.self),
                                         bundle: Bundle(for: PickStickerViewController.self))
    }

    public func chooseFriends() -> ChooseFriendsViewController {
        return ChooseFriendsViewController(nibName: String(describing: ChooseFriendsViewController.self),
                                           bundle: Bundle(for: ChooseFriendsViewController.self))
    }

    public func addFriend() -> AddFriendViewController {
        return AddFriendViewController(nibName: String(describing: AddFriendViewController.self),
                                       bundle: Bundle(for: AddFriendViewController.self))
    }

    public func showQr() -> ShowQrViewController {
        return ShowQrViewController(nibName: String(describing: ShowQrViewController.self),
                                    bundle: Bundle(for: ShowQrViewController.self))
    }

    public func qrScan() -> QrScanViewController {
        return QrScanViewController(nibName: String(describing: QrScanViewController.self),
                                    bundle: Bundle(for: QrScanViewController.self))
    }

    public func newDevice() -> NewDeviceViewController {
        return NewDeviceViewController(nibName: String(describing: NewDeviceViewController.self),
                                       bundle: Bundle(for:NewDeviceViewController.self))
    }

    public func manualCompare() -> ManualCompareViewController {
        return ManualCompareViewController(nibName: String(describing: ManualCompareViewController.self),
                                           bundle: Bundle(for: ManualCompareViewController.self))
    }

    public func intrusion() -> IntrusionViewController {
        return IntrusionViewController(nibName: String(describing: IntrusionViewController.self),
                                       bundle: Bundle(for: IntrusionViewController.self))
    }

    public func verification() -> VerificationViewController {
        return VerificationViewController(nibName: String(describing: VerificationViewController.self),
                                          bundle: Bundle(for: VerificationViewController.self))
    }

    public func photoStream() -> PhotoStreamViewController {
        return PhotoStreamViewController(nibName: String(describing: PhotoStreamViewController.self),
                                         bundle: Bundle(for: PhotoStreamViewController.self))
    }
    
    public func chatBubble(type: BubbleViewType, rect: CGRect) -> CGPath? {
        return BubbleLayer.createPath(
            type: type, frame: rect,
            lineWidth: [BubbleViewType.incoming, BubbleViewType.outgoing].contains(type) ? 2 : 0)
    }
}
