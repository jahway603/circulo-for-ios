//
//  DashView.swift
//  Circulo
//
//  Created by N-Pex on 4.8.21.
//

import UIKit

class VerticalDashView: UIView {
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var dashDivider: CGFloat = 0
    
    var dashBorder: CAShapeLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, dashDivider] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        dashBorder.path = getPath()
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
    
    func getPath() -> CGPath {
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: self.bounds.width / 2, y: 0))
        linePath.addLine(to: CGPoint(x: self.bounds.width / 2, y: self.bounds.height))
        return linePath.cgPath
    }
}

class HorizontalDashView: VerticalDashView {
    override func getPath() -> CGPath {
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: 0,y: self.bounds.height / 2))
        linePath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height / 2))
        return linePath.cgPath

    }
}
