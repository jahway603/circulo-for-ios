//
//  NSMutableAttributedString+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 27.04.23.
//

import Foundation

extension NSMutableAttributedString {

    func range(of aString: any StringProtocol, range: Range<String.Index>? = nil) -> Range<String.Index>? {
        string.range(of: aString, range: range)
    }

    func setAttributes<R>(_ attrs: [NSAttributedString.Key: Any], range: R) where R: RangeExpression, R.Bound == String.Index {
        setAttributes(attrs, range: NSRange(range, in: string))
    }
}
