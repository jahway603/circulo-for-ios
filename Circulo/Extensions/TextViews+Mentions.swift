//
//  TextViews+Mentions.swift
//  Circulo
//
//  Created by N-Pex on 14.01.22.
//

import UIKit
import KeanuCore

fileprivate extension NSAttributedString.Key {
    static let mention = NSAttributedString.Key("Mention")
}

fileprivate extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: frame.size)
        return renderer.image { context in
            layer.render(in: context.cgContext)
        }
    }
}

fileprivate extension NSMutableAttributedString {
    
    func insertMemberAvatar(member: Member, range: NSRange) {
        var font = UIFont.systemFont(ofSize: 20)
        if let f = self.attribute(.font, at: range.lowerBound, effectiveRange: nil) as? UIFont {
            font = f
        }
        let size = CGFloat(font.pointSize)
        
        // Create a mention view, render as image and insert as an attachment
        // into the attributed string.
        let mentionView = MentionView.createNew()
        mentionView.apply(member: member, size: size)
        mentionView.setNeedsLayout()
        mentionView.layoutIfNeeded()
        let mentionImage = mentionView.asImage()
        let a = NSTextAttachment(image: mentionImage)
        
        // Use capHeight to center the image on the line of text.
        a.bounds = CGRect(x: 0, y: (font.capHeight - mentionImage.size.height).rounded() / 2, width: mentionImage.size.width, height: mentionImage.size.height)
        let s = NSMutableAttributedString(attachment: a)
        s.addAttribute(.font, value: font, range: NSRange(location: 0, length: s.length))
        
        // Add custom "mention" attribute, so we can later convert back to plain ole' string...
        s.addAttribute(.mention, value: (member.id, member.displayName), range: NSRange(location: 0, length: s.length))

        // Remove text (user id) and replace with our rendered mention image
        self.replaceCharacters(in: range, with: "")
        self.insert(s, at: range.lowerBound)
    }
}

// From here: https://stackoverflow.com/questions/22507884/uitextfield-get-currently-edited-word
// Modified to return range also.
extension UITextView {
    func showMentions(members: [Member]) {
        var attributedString: NSMutableAttributedString?
        if let at = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: at)
        } else if let t = self.text {
            attributedString = NSMutableAttributedString(string: t, attributes: [.font: self.font as Any])
        }
        if let attributedString = attributedString {
            for member in members {
                do {
                    let regex = try NSRegularExpression(pattern: member.id, options: [.ignoreMetacharacters])
                    for res in regex.matches(in: attributedString.string, options: [], range: NSRange(location: 0, length: attributedString.string.count)) {
                        attributedString.insertMemberAvatar(member: member, range: res.range)
                    }
                }
                catch {}
            }
            self.attributedText = attributedString
        }
    }
    
    func textWithMentions() -> String {
        let attributedString = self.attributedText != nil ? NSMutableAttributedString(attributedString: self.attributedText) : NSMutableAttributedString(string: text, attributes: [.font: self.font as Any])
        attributedString.removeAttribute(.attachment, range: NSRange(location: 0, length: attributedString.length))
        var replacements:[(NSRange,String)] = []
        attributedString.enumerateAttribute(.mention, in: NSRange.init(location: 0, length: attributedString.length), options: []) { (value, range, stop) in
            if let (userId,displayName) = value as? (String,String) {
                let replacement = "<a href='https://matrix.to/#/\(userId)'>\(displayName)</a>"
                replacements.append((range, replacement))
            }
        }
        // Reverse, so don't have to deal with string length changes...
        for (range, replacement) in replacements.reversed() {
            attributedString.replaceCharacters(in: range, with: replacement)
        }
        return attributedString.string
    }
    
    /// Returns the current word that the cursor is at.
    func currentWord() -> (String, UITextRange?) {
        guard let cursorRange = self.selectedTextRange else { return ("", nil) }
        func getRange(from position: UITextPosition, offset: Int) -> UITextRange? {
            guard let newPosition = self.position(from: position, offset: offset) else { return nil }
            return self.textRange(from: newPosition, to: position)
        }
        
        var wordStartPosition: UITextPosition = self.beginningOfDocument
        var wordEndPosition: UITextPosition = self.endOfDocument
        
        var position = cursorRange.start
        
        while let range = getRange(from: position, offset: -1), let text = self.text(in: range) {
            if text == " " || text == "\n" {
                wordStartPosition = range.end
                break
            }
            position = range.start
        }
        
        position = cursorRange.start
        
        while let range = getRange(from: position, offset: 1), let text = self.text(in: range) {
            if text == " " || text == "\n" {
                wordEndPosition = range.start
                break
            }
            position = range.end
        }
        
        guard let wordRange = self.textRange(from: wordStartPosition, to: wordEndPosition) else { return ("", nil) }
        
        return (self.text(in: wordRange) ?? "", wordRange)
    }
}

extension UILabel {
    
    func showMentions(members: [Member]) {
        var attributedString: NSMutableAttributedString?
        if let at = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: at)
        } else if let t = self.text {
            attributedString = NSMutableAttributedString(string: t, attributes: [.font: self.font as Any])
        }
        if let attributedString = attributedString {
            for member in members {
                do {
                    let regex = try NSRegularExpression(pattern: member.id, options: [.ignoreMetacharacters])
                    for res in regex.matches(in: attributedString.string, options: [], range: NSRange(location: 0, length: attributedString.string.count)) {
                        attributedString.insertMemberAvatar(member: member, range: res.range)
                    }
                }
                catch {}
            }
            self.attributedText = attributedString
        }
    }
}
