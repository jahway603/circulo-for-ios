//
//  UIView+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 12.04.24.
//

import UIKit

extension UIView {

    var nextUiViewController: UIViewController? {
        var responder = next

        while (responder != nil) {
            if let vc = responder as? UIViewController {
                return vc
            }

            responder = responder?.next
        }

        return nil
    }

    var heightConstraint: CGFloat? {
        get {
            constraints.filter({ $0.firstAttribute == .height && $0.relation == .equal}).first?.constant
        }
        set {
            constraints.filter({ $0.firstAttribute == .height }).forEach({ $0.isActive = false })

            if let height = newValue {
                heightAnchor.constraint(equalToConstant: height).isActive = true
            }
        }
    }
}
