//
//  MXEvent+Circulo.swift
//  Circulo
//
//  Created by N-Pex on 24.11.21.
//

import MatrixSDK
import KeanuCore
import Localize

public extension MXEvent {

    static let mentionRegex = "<a.*?(@.*?)[\'\"].*?</a>"

    var rawMessageBody: String? {
        if let format = content["format"] as? String,
           format == "org.matrix.custom.html",
           let formattedBody = content["formatted_body"] as? String
        {
            let strippedReply = formattedBody.replacingOccurrences(
                of: "<mx-reply>(.|\n)*</mx-reply>", with: "",
                options: [.literal, .regularExpression], range: nil)

            return strippedReply.replacingOccurrences(
                of: MXEvent.mentionRegex, with: "$1", 
                options: [.literal, .regularExpression], range: nil)
        }

        return content["body"] as? String
    }

    var containsLocation: Bool {
        content[kMXMessageContentKeyExtensibleLocation] != nil ||
        content[kMXMessageContentKeyExtensibleLocationMSC3488] != nil
    }

    var isUrgent: Bool {
        var isUrgent = false

        if !isReply() || sender == originalSender {
            isUrgent = rawMessageBody?.contains("#urgent") ?? false
        }

        return isUrgent
    }
    
    /**
     Based on user settings, decide whether notifications should be shown/hidden for this event. 
     Note that the "master switch" for event notification is alredy applied server side, so here we only consider other settings.
     */
    var showNotification: Bool {
        if eventType == .roomMember {
            // No notifications for avatar or display name updates and for leaves, kicks etc.
            return isOriginalJoin
        }

        if !isReply() && !isInThread() {
            // New status
            switch CirculoSettings.shared.notifyNewStatuses {
            case "always":
                return true

            default: // "urgent"
                return isUrgent
            }
        }
        else {
            // Do not notify location updates for continued location sharing.
            //
            // We would actually need to check the thread root event, if that also
            // contains a location (that's how we identify a continued location sharing
            // thread), but that's expensive and asynchronous, so we throw
            // location events on normal threads under the bus, too!
            if isInThread() && containsLocation {
                return false
            }

            // Updates and responses
            switch CirculoSettings.shared.notifyUpdates {
            case "always":
                return true

            case "mentions":
                guard let name = Circles.shared.account?.userDisplayName,
                      !name.isEmpty
                else {
                    return false
                }

                return rawMessageBody?.contains(name) ?? false

            default:
                return false
            }
        }
    }
    
    var notificationBody: String {
        let getFriendsName = { (userId: String) -> String in
            if let session = Circles.shared.session {
                return FriendsManager.shared.getOrCreate(session.user(withUserId: userId), session).name
            }

            return "somebody".localize()
        }

        if eventType == .roomMember {
            if let displayName = self.content["displayname"] as? String {
                return "% joined your circle".localize(value: displayName)
            } 
            else {
                return "Someone joined your circle".localize()
            }
        }

        if isUrgent {
            return "Alert from %! Open % to see.".localize(values: getFriendsName(sender), Bundle.main.displayName)
        }
        else if !isReply() && !isInThread() {
            if containsLocation {
                return "Keep watch! % started sharing location.".localize(value: getFriendsName(sender))
            }

            return "📣 Someone posted a message.".localize()
        }
        
        let body = rawMessageBody ?? ""

        // Handle Android "#resolved" messages
        if body == "#resolved" {
            return "Someone resolved their status.".localize()
        }

        if eventType == .reaction && body == "👀" {
            return "% sees you. We have confirmation that your alert was viewed.".localize(value: getFriendsName(sender))
        }

        if let account = Circles.shared.account,
           let displayName = account.userDisplayName, body.contains(displayName)
        {
            return "You've been mentioned.".localize()
        } 

        return "Your circle is responding!".localize()
    }
    
    func getNotificationSound(_ criticalAllowed: Bool) -> UNNotificationSound? {
        let defaultSound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "alert1.m4r"))

        if self.eventType == .roomMember {
            return defaultSound
        }

        // Check notification sound settings
        if isUrgent {
            if CirculoSettings.shared.notifyUrgentSound {
                if criticalAllowed {
                    return .defaultCritical
                }

                return defaultSound
            }

            return nil
        }
        else if !CirculoSettings.shared.notifyNormalSound {
            return nil
        }
        
        return defaultSound
    }

    var isJoin: Bool {
        eventType == .roomMember && content?["membership"] as? String == "join"
    }

    /**
     Evaluates, if this an original `join` event.

     Even, if an event is marked as a `join`-event, it can be just an update to avatar or display name.
     Check `prevContent` to determine if this is the original join event!

     From the spec:
     The membership for a given user can change over time.
     The table below represents the various changes over time and how clients and servers must interpret those changes.
     Previous membership can be retrieved from the `prev_content` object on an event.
     If not present, the user’s previous membership must be assumed as leave.
     */
    var isOriginalJoin: Bool {
        isJoin && prevContent?["membership"] as? String != "join"
    }

    var isInvite: Bool {
        eventType == .roomMember && content?["membership"] as? String == "invite"
    }

    var isLeave: Bool {
        eventType == .roomMember && content?["membership"] as? String == "leave"
    }
}

fileprivate extension MXEvent {
    private static let originalSenderRegex = "<(@[A-Z0-9._%#+-:]+)>".r
   
    var originalSender: String? {
        if let body = self.content["body"] as? String, isReply() {
            let sender = MXEvent.originalSenderRegex?.findFirst(in: body)?.group(at: 1)

            print("Original sender is \(sender?.debugDescription ?? "none") for event \(self.debugDescription)")

            return sender
        }

        return nil
    }
}
