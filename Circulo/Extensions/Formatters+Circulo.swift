//
//  Formatters+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 26.06.24.
//

import KeanuCore

extension Formatters {
    
    /**
     Formatter to display a duration human-friendly.
    */
    public static let friendlyDuration: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()

        formatter.unitsStyle = .short
        formatter.allowedUnits = [ .month, .day, .hour, .minute ]
        formatter.zeroFormattingBehavior = [ .dropLeading ]

        return formatter
    }()

    /**
     Format a `TimeInterval` appropriately to display in a human-friendly way.

     - parameter duration: The `TimeInterval` to format.
     - returns: A localized formatted string representation of `duration`.
    */
    public class func format(friendlyDuration: TimeInterval) -> String? {
        return Self.friendlyDuration.string(from: friendlyDuration)
    }

    /**
     Formatter to display media length.
    */
    public static let mediaDuration: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()

        formatter.unitsStyle = .positional
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]

        return formatter
    }()

    /**
     Format a `TimeInterval` appropriately to display for media playback.

     - parameter duration: The `TimeInterval` to format.
     - returns: A localized formatted string representation of `duration`.
    */
    public class func format(mediaDuration: TimeInterval) -> String? {
        return Self.mediaDuration.string(from: mediaDuration)
    }
}
