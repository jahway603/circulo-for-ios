//
//  UIButton+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 29.02.24.
//

import UIKit

extension UIButton {

    func setImage(_ image: UIImage?) {
        setImage(image, for: .application)
        setImage(image, for: .disabled)
        setImage(image, for: .focused)
        setImage(image, for: .highlighted)
        setImage(image, for: .normal)
        setImage(image, for: .reserved)
        setImage(image, for: .selected)
    }
}
