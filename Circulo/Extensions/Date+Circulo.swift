//
//  Date+Circulo.swift
//  Circulo
//
//  Created by N-Pex on 07.06.21.
//

import Foundation

// From: https://stackoverflow.com/questions/44086555/swift-display-time-ago-from-date-nsdate
extension Date {

    var msSinceEpoch: UInt64 {
        UInt64(timeIntervalSince1970 * 1000)
    }

    init(msSinceEpoch: UInt64) {
        self.init(timeIntervalSince1970: msSinceEpoch.timeInterval)
    }

    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: self, relativeTo: Date())
    }
}

extension UInt64 {

    var date: Date {
        Date(msSinceEpoch: self)
    }

    var timeInterval: TimeInterval {
        TimeInterval(self) / 1000
    }
}
