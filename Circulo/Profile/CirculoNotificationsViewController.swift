//
//  CirculoNotificationsViewController.swift
//  Circulo
//
//  Created by N-Pex on 30.11.21.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

class CirculoNotificationsViewController: FormViewController {
    
    // MARK: Properties
    var session: MXSession? {
        didSet {
            if self.isViewLoaded {
                self.createForm()
            }
        }
    }
    
    var notificationEnabled: Bool {
        get {
            if let nc = self.session?.notificationCenter, let rule = nc.rule(byId: ".m.rule.master") {
                return !rule.enabled
            }
            return true
        }
    }
    
    public convenience init() {
        self.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: Public Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Notifications".localize()

        tableView.tintColor = .accent

        createForm()
    }

    open func createForm() {
        form.removeAll()

        form +++ SwitchRow()
        { row in
            row.title = "Notifications".localize()
            row.tag = "Notifications"
            row.value = self.notificationEnabled
            row.cell.textLabel?.numberOfLines = 0
        }
        .onChange { row in
            if let nc = self.session?.notificationCenter {
                if let rule = nc.rule(byId: ".m.rule.master") {
                    nc.enableRule(rule, isEnabled: !(row.value ?? true))
                }
            }
        }
        
        +++ SelectableSection<ListCheckRow<String>>(nil, selectionType: .singleSelection(enableDeselection: false)) { section in
            section.onSelectSelectableRow = { (cell, cellRow) in
                CirculoSettings.shared.notifyNewStatuses = cellRow.value ?? "always"
                section.forEach({ $0.updateCell() })
            }
        }

        <<< LabelRow("newStatus") {
            $0.title = "When to Notify: New Statuses".localize()
            $0.cell.textLabel?.numberOfLines = 0
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = .notifyNew
        })

        for option in [["name":"Anytime someone asks for help".localize(),"value":"always"]
                       ,["name":"Only if it’s urgent".localize(),"value":"urgent"]] {
            form.last! <<< ListCheckRow<String>(option["name"]){ listRow in
                listRow.title = option["name"]
                listRow.selectableValue = option["value"]
                listRow.value = option["value"]
            }
            .cellUpdate({ cell, row in
                if CirculoSettings.shared.notifyNewStatuses == row.value {
                    cell.accessoryView = UIImageView(image: .radioOn)
                } else {
                    cell.accessoryView = UIImageView(image: .radioOff)
                }
            })
            
        }

        
        form +++ SelectableSection<ListCheckRow<String>>(nil, selectionType: .singleSelection(enableDeselection: false)) { section in
            section.onSelectSelectableRow = { (cell, cellRow) in
                CirculoSettings.shared.notifyUpdates = cellRow.value ?? "always"
                section.forEach({ $0.updateCell() })
            }
        }

        <<< LabelRow("updateStatus") {
            $0.title = "When to Notify: Updates & Responses".localize()
            $0.cell.textLabel?.numberOfLines = 0
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = .notifyUpdate
        })

        for option in [["name":"Anytime someone responds".localize(),"value":"always"]
                       ,["name":"Only if I’m mentioned".localize(),"value":"mentions"]
                       ,["name":"Never".localize(),"value":"never"]] {
            form.last! <<< ListCheckRow<String>(option["name"]){ listRow in
                listRow.title = option["name"]
                listRow.selectableValue = option["value"]
                listRow.value = option["value"]
            }
            .cellUpdate({ cell, row in
                if  CirculoSettings.shared.notifyUpdates == row.value {
                    cell.accessoryView = UIImageView(image: .radioOn)
                } else {
                    cell.accessoryView = UIImageView(image: .radioOff)
                }
            })
            
        }
        
        form +++ LabelRow("notifyNewOptions") {
            $0.title = "Customize Círculo Notifications".localize()
            $0.cell.textLabel?.numberOfLines = 0
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = .notifyNormal
            if let section = row.section {
                section.footer = HeaderFooterView(stringLiteral: "If someone posts a new status or responds you will receive a normal Círculo notification.".localize())
            }
        })

        <<< SwitchRow()
        { row in
            row.title = "Sound"
            row.value = CirculoSettings.shared.notifyNormalSound
            row.cell.textLabel?.numberOfLines = 0
        }
        .onChange { row in
            CirculoSettings.shared.notifyNormalSound = row.value ?? true
        }
        
        +++ LabelRow("notifyUrgentOptions") {
            $0.title = "Customize Urgent Notifications".localize()
            $0.cell.textLabel?.numberOfLines = 0
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = .notifyUrgent
            if let section = row.section {
                section.footer = HeaderFooterView(stringLiteral: "If someone makes their status urgent, you will receive an urgent notification.".localize())
            }
        })

        <<< SwitchRow()
        { row in
            row.title = "Sound"
            row.value = CirculoSettings.shared.notifyUrgentSound
            row.cell.textLabel?.numberOfLines = 0
        }
        .onChange { row in
            CirculoSettings.shared.notifyUrgentSound = row.value ?? true
        }
    }
}
