//
//  FeedbackViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 26.04.23.
//

import UIKit
import Eureka
import KeanuCore

class FeedbackViewController: FormViewController {

    private let cim = CleanInsightsManager.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Feedback".localize()

        form
        +++ Section(header: "Measurement Study".localize(), footer: "") {
            $0.tag = "studySection"
        }

        <<< SwitchRow("measurementStudy") {
            $0.title = "Measurement Study".localize()

            $0.cell.textLabel?.numberOfLines = 0
            $0.value = cim.isGranted
            $0.disabled = .function(["measurementStudy"], { _ in
                !CleanInsightsManager.shared.isActive
            })
        }
        .onChange({ [weak self] row in
            if row.value ?? false {
                let vc = UIStoryboard(name: "Main", bundle: nil)
                    .instantiateViewController(identifier: "measurementStudy")

                self?.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self?.cim.deny()
                self?.updateFooter()
            }
        })

        updateFooter()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let row = form.rowBy(tag: "measurementStudy") as? SwitchRow {
            row.value = cim.isGranted
            row.updateCell()
        }

        updateFooter()
    }

    private func updateFooter() {
        form.sectionBy(tag: "studySection")?.footer?.title =  cim.isGranted
        ? "Consent given to measure for % more days.".localize(value: Formatters.format(int: cim.remainingConsentDays))
        : "Help improve the app with confidential usage stats.".localize()

        form.last?.reload()
    }
}
