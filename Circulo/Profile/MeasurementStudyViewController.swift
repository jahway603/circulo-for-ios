//
//  MeasurementStudyViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 26.04.23.
//

import UIKit

class MeasurementStudyViewController: UIViewController {

    private static let check = "✓"


    @IBOutlet weak var li1: UILabel!
    @IBOutlet weak var li2: UILabel!
    @IBOutlet weak var li3: UILabel!
    @IBOutlet weak var li4: UILabel!
    @IBOutlet weak var li5: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        setWithColoredCheck(li1, "% Response time".localize(value: Self.check))
        setWithColoredCheck(li2, "% How long a status is active".localize(value: Self.check))
        setWithColoredCheck(li3, "% Use of conditions (safe, uncertain, etc)".localize(value: Self.check))
        setWithColoredCheck(li4, "% Frequency of location sharing".localize(value: Self.check))
        setWithColoredCheck(li5, "% If messages are sent offline".localize(value: Self.check))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // We're just returning from GroupCodeViewController.
        // So, remove this one, too.
        if CleanInsightsManager.shared.isGranted {
            navigationController?.popViewController(animated: true)
        }
    }


    @IBAction
    func deny() {
        CleanInsightsManager.shared.deny()

        navigationController?.popViewController(animated: true)
    }

    private func setWithColoredCheck(_ label: UILabel, _ text: String) {
        let text = NSMutableAttributedString(string: text)
        var attributes = [NSAttributedString.Key: Any]()

        if let range = text.range(of: Self.check) {
            attributes[.foregroundColor] = UIColor.accent

            if let font = label.font.bold {
                attributes[.font] = font
            }

            text.setAttributes(attributes, range: range)
        }

        label.attributedText = text
    }
}
