//
//  SettingsViewController.swift
//  Circulo
//
//  Created by N-Pex on 12.10.21.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

class SettingsViewController: FormViewController {

    public let languageRow = ButtonRow() {
        $0.title = "Language".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
            cell.tintColor = .accent
    }

    public convenience init() {
        self.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Preferences".localize()

        form +++
            languageRow
            .onCellSelection({ cell, row in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            })
        form +++ LabelRow() {
                $0.title = "Version: %, Build: %".localize(values: Bundle.main.version, Bundle.main.build)
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.font = .italicSystemFont(ofSize: UIFont.smallSystemFontSize)
            }
    }
}
