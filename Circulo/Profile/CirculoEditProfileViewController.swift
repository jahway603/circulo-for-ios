//
//  CirculoEditProfileViewController.swift
//  Circulo
//
//  Created by N-Pex on 08.10.21.
//

import Keanu
import KeanuCore
import UIKit

public protocol CirculoEditProfileDelegate: AnyObject {
    func updateProfile(name: String?, avatar: UIImage?, done: ((Bool) -> Void)?)
}

class CirculoEditProfileViewController: BaseViewController, UITextFieldDelegate, AvatarPickerViewControllerDelegate {

    weak var delegate : CirculoEditProfileDelegate?

    @IBOutlet weak var avatarView: AvatarViewWithStatus!
    @IBOutlet weak var editName: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSave: UIBarButtonItem!
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    // One of these need to be set!
    //
    public var member: Member?
    public var account: MXKAccount?
    
    private var pickedAvatarImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editName.delegate = self
        if let member = member {
            editName.text = member.displayName
            avatarView.member = member
        } else if let account = account {
            editName.text = account.userDisplayName
            avatarView.load(account: account)
        }
        updateSaveButton()
    }

    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.bottomLayoutConstraint.constant = kbSize.height
            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        bottomLayoutConstraint.constant = 0
        animateDuringKeyboardMovement(notification)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 80
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func didChangeName(_ sender: Any) {
        updateSaveButton()
    }
    
    @IBAction func didTapCamera(_ sender: Any) {
        AvatarPicker.showPickerFor(avatarView: self.avatarView, inViewController: self) { image in
            self.pickedAvatarImage = image
            self.updateSaveButton()
        }
    }
    
    @IBAction func didTapAvatar(_ sender: Any) {
        let avatarPicker = AvatarPickerViewController(collectionViewLayout: LeftAlignedCollectionViewFlowLayout())
        avatarPicker.delegate = self
        avatarPicker.modalPresentationStyle = .popover
        avatarPicker.popoverPresentationController?.sourceView = self.avatarView
        avatarPicker.popoverPresentationController?.sourceRect = self.avatarView.bounds
        self.present(avatarPicker, animated: true)
    }

    func didPickAvatar(_ avatarImage: UIImage) {
        if let image = MXKTools.resize(avatarImage, to: CGSize(width: 120, height: 120)) {
            self.pickedAvatarImage = image
            self.avatarView.image = image
            self.updateSaveButton()
        }
    }
    
    func updatedName() -> String? {
        if let member = member {
            if let name = editName?.text, name.count > 0, name != member.displayName {
                return name
            }
        } else if let account = account {
            if let name = editName?.text, name.count > 0, name != account.userDisplayName {
                return name
            }
        }
        return nil
    }
    
    @IBAction func didTapSave() {
        guard let delegate = delegate else { return }
        workingOverlay.isHidden = false
        delegate.updateProfile(name: updatedName(), avatar: pickedAvatarImage, done: { success in
            self.workingOverlay.isHidden = true
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func updateSaveButton() {
        buttonSave.isEnabled = updatedName() != nil || pickedAvatarImage != nil
    }
}
