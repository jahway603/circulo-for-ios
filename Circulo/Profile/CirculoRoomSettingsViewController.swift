//
//  CirculoRoomSettingsViewController.swift
//  Circulo
//
//  Created by N-Pex on 05.10.21.
//

import Keanu
import KeanuCore

public class CirculoRoomSettingsViewController: RoomSettingsViewController {
    override public func configureHeaderRows(room: MXRoom) {
        super.configureHeaderRows(room: room)
        headerRows = headerRows.filter({ $0 != .addFriends && $0 != .alias && /*$0 != .mute && */$0 != .share })
    }
}
