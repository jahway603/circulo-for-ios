//
//  ProfileViewController.swift
//  Circulo
//
//  Created by N-Pex on 25.8.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import CoreLocation

public protocol CirculoProfileViewControllerDelegate: AnyObject {
    func viewOrEditStatus()
}

public class CirculoProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var avatarView: AvatarViewWithStatus!
    @IBOutlet weak var displayNameLb: UILabel!
    @IBOutlet weak var presenceLb: UILabel!

    @IBOutlet weak var editProfileBt: UIButton! {
        didSet {
            editProfileBt.setTitle("Edit Profile".localize().uppercased(with: .current))
        }
    }

    @IBOutlet weak var settingsTableView: UITableView!
    
    weak var delegate : CirculoProfileViewControllerDelegate?

    private var sessionStateObserver: NSObjectProtocol?
    private var memberChangedObserver: NSObjectProtocol?

    // MARK: Properties

    open override func viewDidLoad() {
        super.viewDidLoad()
        settingsTableView.register(ImageButtonCell.nib, forCellReuseIdentifier: ImageButtonCell.defaultReuseId)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateViews()

        let nc = NotificationCenter.default

        sessionStateObserver = nc.addObserver(forName: .mxSessionStateDidChange, object: nil, queue: .main)
        { [weak self] notification in
            self?.updateViews()
        }

        memberChangedObserver = nc.addObserver(forName: .memberChanged, object: nil, queue: .main)
        { [weak self] notification in
            if (notification.object as? Member)?.isMe ?? false {
                self?.updateViews()
            }
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        let nc = NotificationCenter.default

        if let sessionStateObserver = sessionStateObserver {
            nc.removeObserver(sessionStateObserver)
            self.sessionStateObserver = nil
        }

        if let memberChangedObserver = memberChangedObserver {
            nc.removeObserver(memberChangedObserver)
            self.memberChangedObserver = nil
        }
    }


    func updateViews() {
        guard isViewLoaded else {
            return
        }

        if let me = Circles.shared.me() {
            avatarView.member = me
            displayNameLb.text = me.displayName
        }
        else if let myAccount = Circles.shared.account {
            avatarView.load(account: myAccount)
            displayNameLb.text = myAccount.userDisplayName
        }

        // Fix annoying UI bug when switching from/to dark mode.
        editProfileBt.borderColor = .label

        presenceLb.text = Circles.shared.sessionReady ? "Available".localize() : "Offline".localize()
    }


    // MARK: UITableViewDataSource

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath)
        if let cell = cell as? ImageButtonCell {
        switch indexPath.row {

        case 4:
            cell.apply(.feedback, "Feedback".localize())
        
        case 3:
            cell.apply(.about, "About".localize())
            
        case 2:
            cell.apply(.account, "Account".localize())

        case 1:
            cell.apply(.settings, "Preferences".localize())

        default:
            cell.apply(.notifications, "Notifications".localize())

        }
        }
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.row {
        case 4:
            showFeedback()

        case 3:
            showAbout()

        case 2:
            showMe()

        case 1:
            showPreferences()

        default:
            showNotificationSettings()
        }
    }

    open func showNotificationSettings() {
        let vc = CirculoNotificationsViewController()
        navigationController?.pushViewController(vc, animated: true)
    }

    func showPreferences() {
        navigationController?.pushViewController(SettingsViewController(), animated: true)
    }

    func showMe() {
        let meVC = UIApplication.shared.router.profileAccount()
        meVC.showAvatarRow = false
        meVC.showNameRow = false
        meVC.showLinkedIds = false
        meVC.showShareProfile = false
        meVC.showVersion = false
        meVC.account = Circles.shared.account
        navigationController?.pushViewController(meVC, animated: true)
    }
    
    func showAbout() {
        navigationController?.pushViewController(CirculoAboutViewController(), animated: true)
    }

    func showFeedback() {
        navigationController?.pushViewController(FeedbackViewController(), animated: true)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let editProfile = segue.destination as? CirculoEditProfileViewController {
            if let me = Circles.shared.me() {
                editProfile.member = me
            } else {
                editProfile.account = Circles.shared.account
            }
            
            editProfile.delegate = self
        }
    }
}

extension CirculoProfileViewController: CirculoEditProfileDelegate {

    public func updateProfile(name: String?, avatar: UIImage?, done: ((Bool) -> Void)?) {
        guard let account = Circles.shared.account else {
            return
        }

        let updateName = {
            if let name = name {
                account.setUserDisplayName(name) {
                    done?(true)
                } failure: { err in
                    done?(false)
                }
            } 
            else {
               done?(true)
            }
        }

        if let avatar = avatar {
            AvatarPicker.useAsAvatar(account: account, image: avatar, avatarView: nil, done: { success in
                if !success {
                    done?(false)
                } 
                else {
                    updateName()
                }
            })
        } 
        else {
            updateName()
        }
    }
}
