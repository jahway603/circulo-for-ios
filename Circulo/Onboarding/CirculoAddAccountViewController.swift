//
//  CirculoAddAccountViewController.swift
//  Circulo
//
//  Created by N-Pex on 07.04.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import MaterialComponents.MaterialSnackbar
import Foundation

class CirculoAddAccountViewController: AddAccountViewController, CirculoChooseNameViewControllerDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var buttonSkip: UIButton!
    
    private var authenticationManager: AuthenticationManager?
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    private let homeServer = URL(string: "https://\(Config.defaultHomeServer)")!
    private let idServer = URL(string: "https://\(Config.defaultIdServer)")!
    private var username = "Circulo" + String.randomAlphanumericString(length: 6)
    private var password = String.randomAlphanumericString(length: 8)
    private var displayName: String?
    private var displayAvatar: UIImage?
    private var recaptchaSecret = ""
    private var termsAccepted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = nil
        navigationItem.title = ""
        navigationItem.backButtonTitle = "Back"

        authenticationManager = AuthenticationManager(
            self, .register, homeServer: homeServer, idServer: idServer, autoStoreCredentials: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    func createAccountWithName(name: String, avatar: UIImage?) {
        displayName = name
        displayAvatar = avatar
        authenticationManager?.authenticate(username, password, recaptchaSecret, termsAccepted, "", true, nil, displayName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pc = segue.destination as? CirculoChooseNameViewController {
            pc.delegate = self
        }
    }
    
    override func done() {
        navigationController?.popToRootViewController(animated: true)

        // Switch to the main flow. For circulo, don't forward to add friends here.
        UIApplication.shared.startMainFlow(forwardIfNoFriends: false)
        
        // Set avatar. We do this here, because it needs to be done after the "startMainFLow" call above (we need the ALT domains etc to be set up correctly
        // before uploading the avatar)
        if let avatar = displayAvatar, let account = Circles.shared.account {
            AvatarPicker.useAsAvatar(account: account, image: avatar, avatarView: nil, done: nil)
        }

    }
}

extension CirculoAddAccountViewController: AuthenticationManagerDelegate {
    
    func showWorking(_ toggle: Bool) {
        workingOverlay.isHidden = !toggle
    }

    func showRecaptchaTermsToken(_ homeServer: String, _ recaptchaKey: String?, _ terms: MXLoginTerms?, _ tokenNeeded: Bool) {
        present(RecaptchaTermsTokenViewController(homeServer, recaptchaKey, terms, tokenNeeded, delegate: self), animated: true)
    }

    func showError(_ error: String?, _ toggle: Bool) {
        if let error = error {
            let message = MDCSnackbarMessage()
            message.duration = 3
            message.text = error.localize()
            MDCSnackbarManager.default.show(message)
        }
    }
    
    func didLogIn() {
        super.didLogIn(with: username)
    }
}

extension CirculoAddAccountViewController : RecaptchaTermsTokenViewControllerDelegate {
    func success(_ secret: String?, _ regToken: String?) {
        recaptchaSecret = secret ?? ""
        termsAccepted = true
        authenticationManager?.authenticate(username, password, recaptchaSecret, termsAccepted, regToken ?? "", true, nil, displayName)
    }
}
