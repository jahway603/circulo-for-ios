//
//  AvatarPickerViewController.swift
//  Circulo
//
//  Created by N-Pex on 19.11.21.
//

import UIKit

public protocol AvatarPickerViewControllerDelegate: AnyObject {
    func didPickAvatar(_ avatarImage:UIImage)
}

open class AvatarPickerViewController: UICollectionViewController {

    open weak var delegate: AvatarPickerViewControllerDelegate?
    
    private var avatars = [UIImage]()

    fileprivate var assetGridThumbnailSize = CGSize()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(AvatarCell.self, forCellWithReuseIdentifier: "AvatarCell")
        
        for i in 2..<18 {
            if let image = UIImage.avatar(i) {
                avatars.append(image)
            }
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        let cellSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        assetGridThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
    }
    
    // Mark - UICollectionViewDataSource
    
    open override func collectionView(_ collectionView: UICollectionView,
                                      numberOfItemsInSection section: Int) -> Int {
        return avatars.count
    }
    
    open override func collectionView(_ collectionView: UICollectionView,
                                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath)

        if let cell = cell as? AvatarCell {
            cell.imageView.image = self.avatars[indexPath.item]
        }
        return cell
    }

    open override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.dismiss(animated: true)

        delegate?.didPickAvatar(self.avatars[indexPath.item])
        self.dismiss(animated: true, completion: nil)
    }
}

fileprivate class AvatarCell : UICollectionViewCell {
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        self.contentView.addSubview(iv)
        iv.autoPinEdgesToSuperviewEdges()
        return iv
    }()
}
