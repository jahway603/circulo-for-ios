//
//  CreateCircleViewController.swift
//  Circulo
//
//  Created by N-Pex on 16.06.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import MaterialComponents.MaterialSnackbar

public protocol CreateCircleViewControllerDelegate: AnyObject {
    func onRoomCreated(room: MXRoom)
}


public class CreateCircleViewController: UIViewController {
    
    // MARK: Storyboard outlets
    @IBOutlet weak var roomName: UITextField!
    @IBOutlet weak var createButton: UIButton!
    
    public weak var delegate: CreateCircleViewControllerDelegate?
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    /**
     If we have an outstanding network operation that can be canceled, this property will hold on to that.
     */
    private var currentOp: MXHTTPOperation?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        roomName.addTarget(self, action: #selector(self.roomNameDidChange), for: .editingChanged)
        createButton.isHidden = true //Initially hidden
    }
    
    @objc func roomNameDidChange() {
        if roomName.text?.count ?? 0 > 0 {
            createButton.isHidden = false
        } else {
            createButton.isHidden = true
        }
    }
    @IBAction func didPressNext(_ sender: Any) {
        createNewCircle()
    }
    
    func createNewCircle() {
        // Create (empty) Room in the first (default) account.
        if let session = Circles.shared.session {
            // Since cancelling room creation can be tricky, wait 5 seconds before
            // showing the "cancel" button. This will allow the operation to complete
            // under "normal" conditions.
            let workItem = DispatchWorkItem { [weak self] in
                if let self = self, !self.workingOverlay.isHidden {
                    self.workingOverlay.message = "Creating circle... tap to cancel".localize()
                    self.workingOverlay.tapHandler = {
                        if let currentOp = self.currentOp, !currentOp.isCancelled {
                            currentOp.cancel()
                            //self.dismiss()
                        }
                    }
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: workItem)
            
            workingOverlay.message = nil
            workingOverlay.tapHandler = nil
            workingOverlay.isHidden = false
            
            var params:[String:Any] = [:]
            params["visibility"] = "public"
            params["name"] = roomName.text
            params["invite"] = [String]()
            var initialState:[[String:Any]] = [
                ["type": kMXEventTypeStringRoomHistoryVisibility,
                 "content": ["history_visibility": kMXRoomHistoryVisibilityShared]
                ],
                ["type": kMXEventTypeStringRoomGuestAccess,
                 "content": ["guest_access": kMXRoomGuestAccessForbidden]
                ],
                ["type": kMXEventTypeStringRoomJoinRules,
                 "content": ["join_rule": kMXRoomJoinRulePublic]
                ]
            ]
            initialState.append(["type": kMXEventTypeStringRoomEncryption,
                                 "content": ["algorithm": kMXCryptoMegolmAlgorithm]])
            params["initial_state"] = initialState
            params["preset"] = "public_chat"
            
            currentOp = session.createRoom(parameters: params) { (createRoomResponse)  in
                self.currentOp = nil
                workItem.cancel()
                self.workingOverlay.isHidden = true
                if
                    createRoomResponse.isSuccess, let room = createRoomResponse.value {
                    self.delegate?.onRoomCreated(room: room)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.onCreateCircleError()
                }
            }
        } else {
            // TODO - handle error
            onCreateCircleError()
        }
    }

    func onCreateCircleError() {
        // Failed
        let message = MDCSnackbarMessage()
        message.duration = 3
        message.text = "Failed to create circle".localize()
//        message.completionHandler = { userInitiated in
//            self.navigationController?.popViewController(animated: true)
//        }
        MDCSnackbarManager.default.show(message)
    }
}
