//
//  Circles.swift
//  Circulo
//
//  Created by Benjamin Erhart on 13.05.24.
//

import Foundation
import MatrixSDK
import KeanuCore

#if canImport(Keanu)
    import Keanu
#endif

extension Notification.Name {

    static let roomReady = Notification.Name("room-ready")
    static let memberChanged = Notification.Name("member-changed")
    static let memberAdded = Notification.Name("member-added")
}

class Circles {

    enum Errors: LocalizedError {
        case noWorkingSession
        case cannotFindRoom
        case cannotFindThread
        case noAttachment
        case noReadableFile
        case unavailable

        var errorDescription: String? {
            switch self {
            case .noWorkingSession:
                return "No working session.".localize()

            case .cannotFindRoom:
                return "Cannot find room.".localize()

            case .cannotFindThread:
                return "Cannot find thread.".localize()

            case .noAttachment:
                return "No attachment found.".localize()

            case .noReadableFile:
                return "No readable file found.".localize()

            case .unavailable:
                return "Unavailable".localize()
            }
        }
    }

    static let shared = Circles()


    // MARK: Public Properties

    var account: MXKAccount? {
        MXKAccountManager.shared().activeAccounts.first
    }

    var myUserId: String {
        account?.matrixId ?? ""
    }

    var session: MXSession? {
        account?.mxSession
    }

    var sessionReady: Bool {
        account?.mxSession?.state == .running
    }

    @MainActor
    private(set) var rooms = [MXRoom]()


    // MARK: Private Properties

    @MainActor
    private var members = [String: [Member]]()

    @MainActor
    private var timelines = [String: MXEventTimeline]()

    @MainActor
    private var sessionReadyCallbacks = [() -> Void]()

    @MainActor
    private var roomReady = [String: Bool]()


    private init() {
        NotificationCenter.default.addObserver(
            self, selector: #selector(stateDidChange),
            name: .mxSessionStateDidChange, object: nil)
    }


    // MARK: Public Methods

    func reload() async throws {
        await MainActor.run {
            for room in rooms {
                room.close()
            }

            self.rooms.removeAll()
            self.roomReady.removeAll()

            self.timelines.removeAll()

            self.members.removeAll()
        }

        guard session != nil else {
            throw Errors.noWorkingSession
        }

        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) -> Void in
            Task { @MainActor in
                performOnSessionReady {
                    guard self.sessionReady else {
                        return continuation.resume(throwing: Errors.noWorkingSession)
                    }

                    Task { @MainActor in
                        self.rooms = self.session?.rooms.filter({ self.session?.isJoined(onRoom: $0.roomId) ?? false }) ?? []

                        // Make sure we have a push rule for circle joins.
                        if let nc = self.session?.notificationCenter,
                           nc.rule(byId: "circulo.joins") == nil
                        {
                            nc.addOverrideRule(withId: "circulo.joins", conditions: [
                                ["kind": "event_match",
                                 "key": "type",
                                 "pattern": "m.room.member"],
                                ["kind": "event_match",
                                 "key": "content.membership",
                                 "pattern": "join"]
                            ], notify: true, sound: true, highlight: false)
                        }

                        continuation.resume()
                    }
                }
            }
        }

        for room in await rooms {
            await start(room: room)
        }
    }

    @MainActor
    func members(of room: MXRoom?) -> [Member] {
        members(of: room?.roomId)
    }

    @MainActor
    func members(of roomId: String?) -> [Member] {
        members[roomId ?? ""] ?? []
    }

    @MainActor
    func member(of room: MXRoom?, with id: String) -> Member? {
        member(of: room?.roomId, with: id)
    }

    @MainActor
    func member(of roomId: String? = nil, with id: String) -> Member? {
        if let roomId = roomId {
            return members[roomId]?.first(where: { $0.id == id })
        }

        for members in members {
            if let member = members.value.first(where: { $0.id == id }) {
                return member
            }
        }

        return nil
    }

    @MainActor
    func member(having threadId: String) -> Member? {
        for members in members {
            if let member = members.value.first(where: { $0.hasThread(with: threadId) }) {
                return member
            }
        }

        return nil
    }

    @MainActor
    func me(in room: MXRoom?) -> Member? {
        me(in: room?.roomId)
    }

    @MainActor
    func me(in roomId: String? = nil) -> Member? {
        if let roomId = roomId {
            return members[roomId]?.first { $0.isMe }
        }

        for members in members {
            if let me = members.value.first(where: { $0.isMe }) {
                return me
            }
        }

        return nil
    }

    @MainActor
    func room(of member: Member) -> MXRoom? {
        guard let roomId = member.mxMember.originalEvent?.roomId else {
            return nil
        }

        return rooms.first { $0.roomId == roomId }
    }

    @MainActor 
    func isRoomReady(roomId: String) -> Bool {
        return roomReady[roomId] ?? false
    }

    @MainActor 
    func isRoomReady(_ room: MXRoom) -> Bool {
        return isRoomReady(roomId: room.roomId)
    }

    func loadThread(_ threadId: String?) async throws -> [MXEvent] {
        guard let threadId = threadId,
              let thread = session?.threadingService.thread(withId: threadId)
        else {
            throw Errors.cannotFindThread
        }

        let events = try await withCheckedThrowingContinuation { continuation in
            thread.allReplies { response in
                if response.isSuccess, let value = response.value {
                    return continuation.resume(returning: value)
                }

                continuation.resume(throwing: response.error ?? Errors.cannotFindThread)
            }
        }

        return await self.decrypt(events: events, in: thread.roomId)
    }

    func redact(_ event: MXEvent) async throws {
        guard let room = await rooms.first(where: { $0.roomId == event.roomId }) else {
            throw Errors.cannotFindRoom
        }

        return try await withCheckedThrowingContinuation { continuation in
            room.redactEvent(event.eventId, reason: nil) { response in
                if let error = response.error {
                    return continuation.resume(throwing: error)
                }

                continuation.resume()
            }
        }
    }

#if canImport(Keanu)
    func loadAttachment(_ event: MXEvent) async throws -> Data {
        guard let mm = session?.mediaManager,
              let attachment = MXKAttachment(event: event, andMediaManager: mm),
              attachment.contentURL != nil
        else {
            throw Errors.noAttachment
        }

        return try await withCheckedThrowingContinuation { continuation in
            _ = attachment.downloadData { data in
                guard let data = data else {
                    return continuation.resume(throwing: Errors.noAttachment)
                }

                continuation.resume(returning: data)
            } failure: { error in
                continuation.resume(throwing: error ?? Errors.noAttachment)
            } progress: { percentage in
                // Ignored
            }
        }
    }
#endif

    func getReactions(_ event: MXEvent) async throws -> [MXEvent] {
        guard let aggregations = session?.aggregations else {
            throw Errors.unavailable
        }

        let events = try await withCheckedThrowingContinuation { continuation in
            aggregations.reactionsEvents(forEvent: event.eventId, inRoom: event.roomId, from: nil, limit: -1) { response in
                continuation.resume(returning: response.chunk)
            } failure: { error in
                continuation.resume(throwing: error)
            }
        }

        return await decrypt(events: events, in: event.roomId)
    }

    func react(with reaction: String = "👀", to event: MXEvent) async throws {
        try await react(with: reaction, to: event.eventId, in: event.roomId)
    }

    func react(with reaction: String = "👀", to eventId: String, in roomId: String) async throws {
        try await withCheckedThrowingContinuation { continuation in
            session?.aggregations.addReaction(reaction, forEvent: eventId, inRoom: roomId, success: {
                continuation.resume()
            }, failure: { error in
                continuation.resume(throwing: error)
            })
        }
    }

    @discardableResult
    func send(text: String, to room: MXRoom, replyingTo threadId: String? = nil, counter: Int = 0) async throws -> MXEvent {
        do {
            return try await withCheckedThrowingContinuation { continuation in
                // Argl: "[MXRoomSummary] save: Saving room summary should happen from the main thread."
                DispatchQueue.main.async {
                    var echo: MXEvent?

                    room.sendTextMessage(text, threadId: threadId, localEcho: &echo) { response in
                        if response.isSuccess, let id = response.value, let id = id {
                            room.mxSession.event(withEventId: id, inRoom: room.roomId, { response in
                                if response.isSuccess, let event = response.value {
                                    continuation.resume(returning: event)
                                }
                                else {
                                    continuation.resume(throwing: response.error ?? Errors.unavailable)
                                }
                            })
                        }
                        else {
                            continuation.resume(throwing: response.error ?? Errors.unavailable)
                        }
                    }
                }
            }
        }
        catch {
            if await unknownDevicesFixed(error, counter) {
                return try await send(text: text, to: room, replyingTo: threadId, counter: counter + 1)
            }
            else {
                throw error
            }
        }
    }

    func send(location: CLLocation, with name: String, to room: MXRoom, replyingTo threadId: String? = nil, counter: Int = 0) async throws -> String {
        do {
            return try await withCheckedThrowingContinuation { continuation in
                // Argl: "[MXRoomSummary] save: Saving room summary should happen from the main thread."
                DispatchQueue.main.async {
                    room.sendLocation(
                        withLatitude: location.coordinate.latitude,
                        longitude: location.coordinate.longitude,
                        description: name,
                        threadId: threadId,
                        localEcho: nil,
                        assetType: .user)
                    { eventId in
                        if let eventId = eventId {
                            continuation.resume(returning: eventId)
                        }
                        else {
                            continuation.resume(throwing: Errors.unavailable)
                        }
                    } failure: { error in
                        continuation.resume(throwing: error ?? Errors.unavailable)
                    }
                }
            }
        }
        catch {
            if await unknownDevicesFixed(error, counter) {
                return try await send(location: location, with: name, to: room, replyingTo: threadId, counter: counter + 1)
            }
            else {
                throw error
            }
        }
    }

    func send(audio: URL, to room: MXRoom, replyingTo threadId: String? = nil, counter: Int = 0) async throws {
        guard try audio.checkResourceIsReachable(), audio.isFileURL else {
            throw Errors.noReadableFile
        }

        do {
            try await withCheckedThrowingContinuation { continuation in
                var echo: MXEvent?

                room.sendAudioFile(localURL: audio, mimeType: UtiHelper.mimeType(for: audio), threadId: threadId, localEcho: &echo) { response in
                    if response.isSuccess {
                        continuation.resume()
                    }
                    else {
                        continuation.resume(throwing: response.error ?? Errors.unavailable)
                    }
                }
            }
        }
        catch {
            if await unknownDevicesFixed(error, counter) {
                try await send(audio: audio, to: room, replyingTo: threadId, counter: counter + 1)
            }
            else {
                throw error
            }
        }
    }

    func joinRoom(roomIdOrAlias: String) async throws -> MXRoom {
        guard let session = session else {
            throw Errors.cannotFindRoom
        }

        return try await withCheckedThrowingContinuation { continuation in
            // Argl: "[MXRoomSummary] save: Saving room summary should happen from the main thread."
            DispatchQueue.main.async {
                session.joinRoom(roomIdOrAlias, completion: { response in
                    if let room = response.value {
                        return continuation.resume(returning: room)
                    }
                    
                    continuation.resume(throwing: response.error ?? Errors.cannotFindRoom)
                })
            }
        }
    }


    // MARK: Private Methods

    @objc
    private func stateDidChange(_ notification: Notification? = nil) {
        guard sessionReady || session?.state == .initialSyncFailed else {
            return
        }

        // The session can end up in this really ugly loop, where it cannot
        // start anymore, but will continuously try to and end up in this
        // state again and again. So, stop this here.
        // The UI will need to diplay information and help the user remedy,
        // e.g. by logging out and re-login.
        if session?.state == .initialSyncFailed {
            account?.closeSession(true)
            NotificationCenter.default.post(name: .syncingStopped, object: nil)
        }

        Task { @MainActor in
            while !sessionReadyCallbacks.isEmpty {
                sessionReadyCallbacks.removeFirst()()
            }
        }
    }

    /**
     This will also perform on `.initialSyncFailed` , to break out of a never-ending wait.

     This can happen, if a user was kicked out of a room.
     */
    @MainActor
    private func performOnSessionReady(_ callback: @escaping () -> Void) {
        sessionReadyCallbacks.append(callback)

        if sessionReady || session?.state == .initialSyncFailed {
            stateDidChange()
        }
    }

    private func unknownDevicesFixed(_ error: Error, _ counter: Int) async -> Bool {
        let error = error as NSError

        guard counter < 1 &&
              error.domain == MXEncryptingErrorDomain &&
              error.code == MXEncryptingErrorUnknownDeviceCode.rawValue,
              let devices = error.userInfo[MXEncryptingErrorUnknownDeviceDevicesKey] as? MXUsersDevicesMap<MXDeviceInfo>,
              let crypto = session?.crypto as? MXLegacyCrypto
        else {
            return false
        }

        return await withCheckedContinuation { continuation in
            crypto.setDevicesKnown(devices) {
                continuation.resume(returning: true)
            }
        }
    }

    private func getAllThreads(in roomId: String) async throws -> [any MXThreadProtocol] {
        guard let threadingService = session?.threadingService else {
            throw Errors.cannotFindThread
        }

        return try await withCheckedThrowingContinuation { continuation in
            threadingService.allThreads(inRoom: roomId) { response in
                if response.isSuccess, let value = response.value {
                    continuation.resume(returning: value)
                }
                else {
                    continuation.resume(throwing: response.error ?? Errors.cannotFindThread)
                }
            }
        }
    }

    private func decrypt(events: [MXEvent], in roomId: String) async -> [MXEvent] {
        guard events.contains(where: { $0.isEncrypted && $0.clear == nil && !$0.isEncrypedAndSentBeforeWeJoined() }),
              let session = session,
              let timelineId = await timelines[roomId]?.timelineId
        else {
            return events
        }

        return await withCheckedContinuation { continuation in
            session.decryptEvents(events, inTimeline: timelineId, onComplete: { decrypted in
                let result = events.map { e in decrypted?.first(where: { $0.eventId == e.eventId }) ?? e }

                continuation.resume(returning: result)
            })
        }
    }

    private func start(room: MXRoom) async {
        // Since room.members() loads the timeline anyway, we do that ourselves, first.
        let timeline = await withCheckedContinuation { continuation in
            room.liveTimeline { timeline in
                continuation.resume(returning: timeline)
            }
        }

        var members = [MXRoomMember]()
        var count = 0

        // This will probably fail the first time right after we joined. So let's try again.
        while members.isEmpty && count < 3 {
            do {
                members = try await withCheckedThrowingContinuation { continuation in
                    room.members { response in
                        if let members = response.value??.members {
                            return continuation.resume(returning: members)
                        }

                        continuation.resume(throwing: response.error ?? Errors.unavailable)
                    }
                }
            }
            catch {
                print("[\(String(describing: type(of: self)))] error=\(error)")
            }

            count += 1
        }

        await self.remove(from: room.roomId)

        // Add only invited and joined, and only add once!
        for member in members {
            if member.membership == .invite || member.membership == .join {
                await self.add(member: Member(member), to: room.roomId)
            }
        }

        Task { @MainActor in
            // MXEventTimeline.listenToEvents, .removeListener and .removeAllListeners need to run
            // on the main thread as these modify the MXEventTimeline.eventListeners property which
            // is copied in MXEventTimeline.notifyListeners in an thread-unsafe way.

            _ = timeline?.listenToEvents([.roomMessage, .roomMember, .roomRedaction, .roomName, .reaction]) { event, dir, state in
                Task {
                    let event = await self.decrypt(events: [event], in: room.roomId).first!

                    await self.onEvent(event, in: room, dir: dir, state: state)
                }
            }

            timeline?.resetPagination()
            timeline?.paginate(20, direction: .backwards, onlyFromStore: false) { response in
                self.onPaginate(room.roomId, timeline!)
            }

            if let old = self.timelines[room.roomId] {
                old.removeAllListeners()
            }

            self.timelines[room.roomId] = timeline
        }
    }

    private func onEvent(_ event: MXEvent, in room: MXRoom, dir: MXTimelineDirection, state: MXRoomState?) async
    {
        if event.isEncrypedAndSentBeforeWeJoined() {
            return
        }

        if event.eventType == .reaction {
            if !event.isRedactedEvent(),
               let eventId = event.relatesTo?.eventId,
               let member = await member(having: eventId)
            {
                if (try? await member.add(event: event)) ?? false {
                    await notifyMemberChanged(member, dir: dir)
                }
            }

            return
        }

        if let member = await member(of: room, with: event.sender) {
            if (try? await member.add(event: event)) ?? false {

                // Uh-oh. This was a leave event.
                if member.leave != nil {
                    if member.isMe {
                        // If the user left the room, rebuild everything.
                        Task {
                            do {
                                try await reload()
                            }
                            catch {
                                print("[\(String(describing: type(of: self)))] reload error=\(error)")
                            }

                            await notifyMemberChanged(member, dir: dir)
                        }
                    }
                    else {
                        // If it's another user, just remove them from the room.
                        await remove(member: member, from: room.roomId)
                        await notifyMemberChanged(member, dir: dir)
                    }
                }
                else {
                    await notifyMemberChanged(member, dir: dir)
                }
            }

            return
        }

        // We only look for forward room joins here to catch new members.
        // We try really hard to load the room member list upfront. It's a pain in the ass to
        // try to cobble together the room member list by paging backwards and honoring all the leaves and joins.
        // Let's not go there.
        if dir == .forwards
            && !event.isRedactedEvent()
            && event.isJoin // `Member` can only be created from a `.roomMember` event.
        {
            let member = Member(event)

            await add(member: member, to: room.roomId)

            await MainActor.run {
                NotificationCenter.default.post(name: .memberAdded, object: member)
            }
        }
    }

    /**
     If all members are properly initialized, will load all location and the last alert thread of each member.

     Else will continue backpaging until satisfied or impossible to further backpage.
     */
    private func onPaginate(_ roomId: String, _ timeline: MXEventTimeline) {
        Task { @MainActor in
            if (members[roomId]?.allSatisfy({ $0.isInitialized }) ?? false) && timeline.canPaginate(.backwards) {
                timeline.paginate(20, direction: .backwards, onlyFromStore: false) { response in
                    self.onPaginate(roomId, timeline)
                }

                return
            }

            if let threads = try? await getAllThreads(in: roomId) {
                for thread in threads {
                    guard let event = thread.rootMessage,
                          !event.isRedactedEvent(),
                          let member = member(of: roomId, with: event.sender)
                    else {
                        continue
                    }

                    _ = try? await member.add(event: event, load: false)
                }

                for member in members[roomId] ?? [] {
                    try? await member.load()
                }
            }

            self.roomReady[roomId] = true

            NotificationCenter.default.post(name: .roomReady, object: self.rooms.first(where: { $0.roomId == roomId }))
        }
    }

    /**
     Add a given `Member` to the room member list.

     If the member already exists, the new one will be ignored!

     - parameter member: The `Member` to add.
     - parameter roomId: The ID of the room to add the `Member` to.
     */
    @MainActor
    private func add(member: Member, to roomId: String) {
        if members[roomId] == nil {
            members[roomId] = []
        }

        if !members[roomId]!.contains(member) {
            members[roomId]?.append(member)
        }
    }

    @MainActor
    private func remove(member: Member, from roomId: String) {
        remove(memberId: member.id, from: roomId)
    }

    @MainActor
    private func remove(memberId: String? = nil, from roomId: String) {
        if let memberId = memberId {
            members[roomId]?.removeAll(where: { $0.id == memberId })
        }
        else {
            members[roomId]?.removeAll()
        }
    }

    @MainActor
    private func notifyMemberChanged(_ member: Member, dir: MXTimelineDirection) {
        // Don't notify while reading old stuff. Otherwise, race conditions happen,
        // e.g. where other parts of the code think, "hey, there is an open location share,
        // we need to restart that!".
        guard dir != .backwards else {
            return
        }
        NotificationCenter.default.post(name: .memberChanged, object: member)
    }
}
