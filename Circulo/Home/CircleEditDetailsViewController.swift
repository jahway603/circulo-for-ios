//
//  CircleEditDetailsViewController.swift
//  Circulo
//
//  Created by N-Pex on 11.10.21.
//

import Keanu
import KeanuCore
import UIKit

public protocol CircleEditDetailsViewControllerDelegate: AnyObject {
    func updateDetails(name: String?, topic: String?, done: ((Bool) -> Void)?)
}

class CircleEditDetailsViewController: BaseViewController, UITextFieldDelegate {

    weak var delegate : CircleEditDetailsViewControllerDelegate?

    @IBOutlet weak var editName: UITextField!
    @IBOutlet weak var editTopic: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSave: UIBarButtonItem!
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    public var circle: MXRoom?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editName.delegate = self
        editTopic.delegate = self
        if let circle = circle {
            editName.text = circle.summary.displayName
            editTopic.text = circle.summary.topic
        }
        updateSaveButton()
    }

    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.bottomLayoutConstraint.constant = kbSize.height
            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        bottomLayoutConstraint.constant = 0
        animateDuringKeyboardMovement(notification)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func didChangeName(_ sender: Any) {
        updateSaveButton()
    }

    @IBAction func didChangeTopic(_ sender: Any) {
        updateSaveButton()
    }
    
    func updatedName() -> String? {
        if let circle = circle, let name = editName?.text, name.count > 0, name != circle.summary.displayName {
            return name
        }
        return nil
    }

    func updatedTopic() -> String? {
        if let circle = circle, let topic = editTopic?.text, topic.count > 0, topic != circle.summary.topic {
            return topic
        }
        return nil
    }

    @IBAction func didTapSave() {
        guard let delegate = delegate else { return }
        workingOverlay.isHidden = false
        delegate.updateDetails(name: updatedName(), topic: updatedTopic(), done: { success in
            self.workingOverlay.isHidden = true
            if success {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func updateSaveButton() {
        buttonSave.isEnabled = updatedName() != nil || updatedTopic() != nil
    }
}
