//
//  HomeDataSource.swift
//  Circulo
//
//  Created by N-Pex on 26.04.21.
//

import KeanuCore
import Keanu
import MatrixSDK

extension MXEvent {
    
    /**
     IFF this is a reply, return the original message id.
     */
    var originalMessageId: String? {
        get {
            if
                let relatesTo = content["m.relates_to"] as? [String:Any],
                let inReplyTo = relatesTo["m.in_reply_to"] as? [String:Any] {
                return inReplyTo["event_id"] as? String
            }
            else if eventType == __MXEventType.reaction,
                let relatesTo = content["m.relates_to"] as? [String:Any] {
                return relatesTo["event_id"] as? String
            }
            return nil
        }
    }
}

protocol HomeDataSourceDelegate: class {
    func didChange(deleted:[IndexPath]?, added:[IndexPath]?, changed:[IndexPath]?, live:Bool, disableScroll:Bool)
    func createCell(at indexPath: IndexPath, for messageData: ChannelMessageData) -> UITableViewCell?
    func hasInvites(_ hasInvites:Bool)
}

class HomeDataSource: AggregatedRoomSummaries, UITableViewDataSource {
    /**
     Map between roomId and the "current message". This is different form the "roomSummary.lastEvent" in that the
     latter can be a REPLY, but we want to keep track of the original message!
     */
    private var messages:SortedArray<ChannelMessageData> = SortedArray<ChannelMessageData>()
    private var eventMessageMap:[String:ChannelMessageData] = [:]
    private var eventListeners:[MXSession:Any] = [:]
    private var reactionListeners:[String:Any] = [:]
    
    public weak var delegate: HomeDataSourceDelegate?
    
    override func didAddRoomSummary(_ roomSummary: MXRoomSummary) {
        if eventListeners[roomSummary.mxSession] == nil {
            // Register a listener for this session
            //
            eventListeners[roomSummary.mxSession] = roomSummary.mxSession.listenToEvents([.roomMessage]) { (event, direction, roomState) in
                
                // Ignore events from rooms we are not joined to (if any should appear...)
                if roomSummary.isInvite {
                    return
                }
                
                // Only consider original events
                if let originalMessageId = event.originalMessageId {
                    self.onEventUpdated(originalMessageId)
                    return
                }
                
                DispatchQueue.main.async {
                    if self.eventMessageMap[event.eventId] != nil {
                        // Already present, mark as updated
                        self.onEventUpdated(event.eventId)
                    } else if
                        !event.isLocalEvent(),
                        let room = self.roomSummaryFromRoomId(event.roomId),
                        let localEchoEvent = room.room.pendingLocalEchoRelated(to: event),
                        let messageData = self.eventMessageMap[localEchoEvent.eventId] {
                        // The "real" event that replaces a local echo!
                        self.eventMessageMap.removeValue(forKey: localEchoEvent.eventId)
                        event.unsignedData = localEchoEvent.unsignedData
                        messageData.event = event
                        messageData.roomState = roomState as? MXRoomState
                    } else {
                        // Add to list
                        let newMessageData = ChannelMessageData()
                        newMessageData.channel = self.roomSummaryFromRoomId(event.roomId)
                        newMessageData.roomState = roomState as? MXRoomState
                        newMessageData.event = event
                        let index = self.messages.insert(newMessageData)
                        self.eventMessageMap[event.eventId] = newMessageData
                        self.delegate?.didChange(deleted: nil, added: [IndexPath(row: index, section: 0)], changed: nil, live: direction == .forwards, disableScroll: false)
                    }
                }
            }
        }
        
        roomSummary.room.liveTimeline { (timeline) in
            guard let timeline = timeline else {return}
            timeline.resetPagination()
            if timeline.canPaginate(.backwards) {
                timeline.paginate(timeline.remainingMessagesForBackPaginationInStore(), direction: .backwards, onlyFromStore: true) { (response) in
                    // done
                }
            }
        }

        // Add a reaction listener
        //
       let listener =  roomSummary.mxSession.aggregations.listenToReactionCountUpdate(inRoom: roomSummary.roomId) { (changes:[String : MXReactionCountChange]) in
            for change in changes {
                self.onEventUpdated(change.key)
            }
        }
        reactionListeners[roomSummary.roomId] = listener
        updateNotificationIndicator()
    }
    
    override func didRemoveRoomSummary(_ roomSummary: MXRoomSummary) {
        if let listener = reactionListeners[roomSummary.roomId] {
            roomSummary.mxSession.aggregations.removeListener(listener)
            reactionListeners.removeValue(forKey: roomSummary.roomId)
        }
        updateNotificationIndicator()
        
        DispatchQueue.main.async {
            // Remove messages
            var indexPathsRemoved:[IndexPath] = []
            for i in 0..<self.messages.count {
                if let s = self.messages[i].channel, s.roomId == roomSummary.roomId {
                    indexPathsRemoved.append(IndexPath(row: i, section: 0))
                }
            }
            self.messages = self.messages.filter { (channelMessageData) -> Bool in
                if let s = channelMessageData.channel, s.roomId == roomSummary.roomId {
                    return false
                }
                return true
            }
            self.delegate?.didChange(deleted: indexPathsRemoved, added: nil, changed: nil, live: false, disableScroll: true)
        }
    }

    override func didChangeRoomSummary(_ roomSummary: MXRoomSummary) {
        updateNotificationIndicator()
    }

    override func didChangeAllRoomSummaries() {
        updateNotificationIndicator()
    }
    
    override func shouldAddRoomSummary(_ roomSummary: MXRoomSummary) -> Bool {
        if roomSummary.isArchived {
            return false
        }
        if roomSummary.membership == .join || roomSummary.membership == .invite {
            return true
        }
        return false
    }
    
    func roomSummaryFromRoomId(_ roomId: String) -> MXRoomSummary? {
        return self.roomSummaries.first(where: { $0.roomId == roomId})
    }
    
    func onEventUpdated(_ eventId: String) {
        DispatchQueue.main.async {
            if
                let channelMessageData = self.eventMessageMap[eventId],
                let index = self.messages.index(of: channelMessageData) {
                self.delegate?.didChange(deleted: nil, added: nil, changed: [IndexPath(row: index, section: 0)], live: false, disableScroll: true)
            }
        }
    }
    
    /**
     Check if we have pending invites. In that case show a small indicator "dot" on the notification icon.
     */
    func updateNotificationIndicator() {
        let hasInvites = roomSummaries.first { (summary) -> Bool in
            summary.isInvite
        } != nil
        delegate?.hasInvites(hasInvites)
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let delegate = self.delegate, let cell = delegate.createCell(at: indexPath, for: messages[indexPath.row]) {
            return cell
        }
        return UITableViewCell() // Fallback to avoid crash
    }
    
    open func messageFor(indexPath: IndexPath) -> ChannelMessageData? {
        if indexPath.row >= 0, indexPath.row < messages.count {
            return messages[indexPath.row]
        }
        return nil
    }
}
