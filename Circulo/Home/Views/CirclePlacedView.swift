//
//  CirclePlacedView.swift
//  Circulo
//
//  Created by N-Pex on 18.8.21.
//

import UIKit

/**
 A view that places itself on a circle within the parent view. You set the size, angle and radius of the view (relative to parent)
 */
open class CirclePlacedView: UIView {
    
    var size: Double = 0.15
    var angle: Double = 0
    var radius: Double = 0.9

    private let layoutConstraints = NSMutableArray()


    open override func layoutSubviews() {
        super.layoutSubviews()

        guard let superview = superview else {
            return
        }

        let parentSize = min(superview.bounds.width, superview.bounds.height)
        let childSize = parentSize * CGFloat(size)
        let r = radius * ((Double)(parentSize - childSize) / 2.0)

        var x = r * cos((angle * .pi) / 180)
        var y = r * -sin((angle * .pi) / 180)

        x += Double(superview.bounds.midX - (childSize / 2.0))
        y += Double(superview.bounds.midY - (childSize / 2.0))

        layoutConstraints.autoRemoveConstraints()
        layoutConstraints.removeAllObjects()

        if superview.bounds.height >= superview.bounds.width {
            layoutConstraints.add(
                autoMatch(.width, to: .width, of: superview, withMultiplier: CGFloat(size)))
        }
        else {
            layoutConstraints.add(
                autoMatch(.height, to: .height, of: superview, withMultiplier: CGFloat(size)))
        }

        layoutConstraints.add(
            autoPinEdge(.leading, to: .leading, of: superview, withOffset: CGFloat(x)))
        layoutConstraints.add(
            autoPinEdge(.top, to: .top, of: superview, withOffset: CGFloat(y)))
    }
}
