//
//  EllipticalView.swift
//  Circulo
//
//  Created by N-Pex on 18.8.21.
//

import UIKit

class EllipticalView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        let ovalPath = UIBezierPath(ovalIn: self.bounds)
        let maskLayer = CAShapeLayer()
        maskLayer.path = ovalPath.cgPath
        layer.mask = maskLayer
    }
}
