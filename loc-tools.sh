#!/bin/sh

#  loc-tools.sh
#  Circulo
#
#  Created by Benjamin Erhart on 13.04.23.
#  
BASE=$(dirname "$0")

export LOC_TOOLS_BASE=$BASE

cat "$BASE/LocTools.swift" \
    "$BASE/loc-tools.swift" \
    | /usr/bin/env xcrun --sdk macosx swift -
